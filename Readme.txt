                           x0r_jmp PRESENTS:
 
 �����������    ������        ���������      ���������   �����������
 �������������  ������       ������������  ������������� �������������
 �������������� ������      �������������������������������������������
 ������� �������������     ������� �������������� ������������� �������
 �������������� ������     �������  ������������� �������������  ������
 �������������  ������     �������  �������������  ������������  ������
 �������������� ������     �������  �������������  ������������  ������
 ������� �������������     �������  ������������� �������������  ������
 ������� �������������������������  ������������� ������������� �������
 ������������������������������������������������������� �������������
 �������������� ������������ ������������  ������������� �������������
 ������������   ������������  ����������     ���������   �����������
 ��                                          �                   ��
 �                                                                �
                    SOURCE CODE BACKUP (1996-07-22)               �
                                                                  �
                           ���������������ͻ
 �������������������������͹ RELEASE NOTES �������������������������ͻ
 �                         ���������������ͼ                         �
 �                                                                   �
 � This release contains a backup of Blood's source code from July   �
 � 22, 1996. No binaries or assets necessary to run the game are     �
 � included in the distribution.                                     �
 �                                                                   �
 � A small group of non-critical files were modified in the archive  �
 � we originally received. The following files were affected and do  �
 � not retain their original modification dates as a result:         �
 �                                                                   �
 � * blood\BLOOD.CFG                                                 �
 � * blood\SHPLAY.INI                                                �
 � * blood\VGA.DAT                                                   �
 � * qtools\VGA.DAT                                                  �
 �                                                                   �
 � The password for the BSOURCE.ZIP archive is 'damnation'.          �
 �������������������������������������������������������������������ͼ
