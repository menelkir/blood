/*******************************************************************************
	FILE:			TRIG.H

	DESCRIPTION:	Contains trigonometry definitions and functions

	AUTHOR:			Peter M. Freese
	CREATED:		08-10-95
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/

#ifndef __TRIG_H
#define __TRIG_H

#pragma library(qtools)

#include "resource.h"

// Angle definitions
#define kAngle360		2048
#define kAngleMask		(kAngle360 - 1)
#define kAngle270		(kAngle180 + kAngle90)
#define kAngle180		(kAngle360 / 2)
#define kAngle120		(kAngle360 / 3)
#define kAngle90		(kAngle360 / 4)
#define kAngle60		(kAngle360 / 6)
#define kAngle45		(kAngle360 / 8)
#define kAngle30		(kAngle360 / 12)
#define kAngle20		(kAngle360 / 18)
#define kAngle15		(kAngle360 / 24)
#define kAngle10		(kAngle360 / 36)
#define kAngle5			(kAngle360 / 72)


extern long costable[kAngle360];


inline long Sin(int angle)
{
	return costable[(angle - kAngle90) & kAngleMask];
}


inline long Cos(int angle)
{
	return costable[angle & kAngleMask];
}

int GetOctant( int dx, int dy );
void RotateVector( long *x, long *y, int angle );
void RotatePoint(long *x, long *y, int angle, int xorg, int yorg);

void trigInit( const Resource &resource );

#endif

