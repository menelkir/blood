/*******************************************************************************
	FILE:			ERROR.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __ERROR_H
#define __ERROR_H

#pragma library(qtools)

/*
enum ErrorResult
{
	ER_IGNORE,
	ER_RETRY,
};

enum ErrorSeverity
{
	ES_WARNING,
	ES_ERROR,
};
*/

enum ErrorFlags
{
	EF_NONE = 0,
	EF_CANRETRY = 0x01,
	EF_CANIGNORE = 0x02,
};

struct Error
{
	const char *module;
	int line;
	const char *description;
	ErrorFlags flags;
	int subCode;
	Error(const char *module, int line, const char *description, ErrorFlags flags = EF_NONE) :
		module(module), line(line), description(description), flags(flags)
		{};
};

typedef void (*EHF)( const Error& error );

EHF errSetHandler( EHF ehf );
void _ThrowError( const char *__format, ... );
void _SetErrorLoc( const char *module, int line );
#define ThrowError _SetErrorLoc(__FILE__, __LINE__), _ThrowError

#endif

