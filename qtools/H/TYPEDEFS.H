/*******************************************************************************
	FILE:			TYPEDEFS.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __TYPEDEFS_H
#define __TYPEDEFS_H

/***********************************************************************
 * Global typedefs
 **********************************************************************/
typedef unsigned char 	uchar;
typedef signed char   	schar;

typedef unsigned short 	ushort;
typedef signed short 	sshort;

typedef unsigned long 	ulong;
typedef signed long   	slong;

typedef unsigned char  	BYTE;
typedef unsigned short 	WORD;
typedef unsigned long  	DWORD;

typedef unsigned char	BOOL;

#define kTrue			(0 == 0)
#define kFalse			(0 == 1)

#define TRUE			kTrue
#define FALSE			kFalse

typedef void (interrupt far * HANDLER)();

#endif
