/*******************************************************************************
	FILE:			TEXTIO.CPP

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "textio.h"
#include "misc.h"

/***********************************************************************
 * Manifest constants
 **********************************************************************/
#define kAttrDefault	(kColorBlack * 16 | kColorLightGray)

#define kGaugeLen		32

/***********************************************************************
 * Local variables
 **********************************************************************/
int tioScreenRows, tioScreenCols;
int tioStride;

static struct {
	int row, col;
	char top, bottom;
} cursor;

static struct {
	int top, left, rows, cols;
} window;

static char nAttribute;

static char *Frames[] =
{
    "�Ŀ� ����",
    "�ͻ� ��ͼ",
	"���� ����",
};

char ShadeTable[256]={
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x88,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
	0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x18,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,
	0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x28,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,
	0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x38,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,
	0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x48,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,
	0x50,0x50,0x50,0x50,0x50,0x50,0x50,0x58,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,
	0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x68,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,
	0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x78,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77
};


/***********************************************************************
 * Scroll()
 *
 * Scroll the specified screen region.  If lines == 0, the area
 * specified is cleared.
 **********************************************************************/
void Scroll( int lines, char attr, int top, int left, int bottom, int right );
#pragma aux Scroll =\
	"mov	ah,0x06",\
    "int    0x10,"\
	parm [al] [bh] [ch] [cl] [dh] [dl]


/***********************************************************************
 * SetCursorPos()
 *
 * Set the BIOS cursor position.  This is useful to keep the textio
 * system and the BIOS in sync.
 **********************************************************************/
void SetCursorPos( int row, int column);
#pragma aux SetCursorPos =\
	"mov	ah,0x02",\
	"mov	bh,0",\
    "int    0x10,"\
	parm [dh] [dl] \
	modify [bh]

/***********************************************************************
 * SetCursorSize()
 **********************************************************************/
void SetCursorSize( char top, char bottom);
#pragma aux SetCursorSize =\
	"mov	ah,0x01",\
    "int    0x10,"\
	parm [ch] [cl]


static inline char *VideoAddress( int row, int col )
{
	return (char *)0xB8000 + (row * tioScreenCols + col) * 2;
}


/***********************************************************************
 * WriteString()
 *
 * Write a string and attributes to the display
 **********************************************************************/
static void WriteString( int row, int col, char *string, char attr )
{
	char *vidmem = VideoAddress(row, col);

	for ( ; *string; string++ )
	{
		*vidmem++ = *string;
		*vidmem++ = attr;
	}
}


/***********************************************************************
 * tioInit()
 **********************************************************************/
void tioInit( int flags )
{
	// get screen size from BIOS Data Area
	tioScreenCols = *(short *)0x44A;
	tioScreenRows = *(char *)0x484 + 1;
	tioStride = tioScreenCols * 2;

	// get cursor size from BIOS Data Area
	cursor.top = *(char *)0x460;
	cursor.bottom = *(char *)0x461;

	cursor.col = *(char *)0x450;
	cursor.row = *(char *)0x451;

	tioCursorOff();

	tioSetAttribute(kAttrDefault);
	tioWindow(0, 0, tioScreenRows, tioScreenCols);

	if ( flags & kInitClear )
		tioClearWindow();
}


/***********************************************************************
 * tioTerm()
 **********************************************************************/
void tioTerm( void )
{
	tioCursorOn();
}


/***********************************************************************
 * tioCursorOff()
 **********************************************************************/
void tioCursorOff()
{
	SetCursorSize(0x20, 0);
}


/***********************************************************************
 * tioCursorOn()
 **********************************************************************/
void tioCursorOn()
{
	SetCursorSize(cursor.top, cursor.bottom);
}


/***********************************************************************
 * tioFill()
 *
 * Fill a rectangle with the specified character and attribute
 **********************************************************************/
void tioFill( int top, int left, int rows, int cols, char c, char attr )
{
	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left);

	for( int y = 0; y < rows; y++ )
	{
		for( int x = 0; x < cols; x++ )
		{
			*vidmem++ = c;
			*vidmem++ = attr;
		}
		vidmem += tioStride - cols * 2;
	}
}


/***********************************************************************
 * tioFillChar()
 *
 * Fill a rectangle with the specified character
 **********************************************************************/
void tioFillChar( int top, int left, int rows, int cols, char c )
{
	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left);

	for ( int y = 0; y < rows; y++ )
	{
		for ( int x = 0; x < cols; x++ )
		{
			*vidmem = c;
			vidmem += 2;
		}
		vidmem += tioStride - cols * 2;
	}
}


/***********************************************************************
 * tioFillAttr()
 *
 * Fill a rectangle with the specified attribute
 **********************************************************************/
void tioFillAttr( int top, int left, int rows, int cols, char attr )
{
	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left) + 1;

	for ( int y = 0; y < rows; y++ )
	{
		for ( int x = 0; x < cols; x++ )
		{
			*vidmem = attr;
			vidmem += 2;
		}
		vidmem += tioStride - cols * 2;
	}
}


/***********************************************************************
 * tioFillShadow()
 *
 * Fill a rectangle with the shadow attribute
 **********************************************************************/
void tioFillShadow( int top, int left, int rows, int cols )
{
	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left) + 1;

	for ( int y = 0; y < rows; y++ )
	{
		for ( int x = 0; x < cols; x++ )
		{
			*vidmem = ShadeTable[*vidmem];
			vidmem += 2;
		}
		vidmem += tioStride - cols * 2;
	}
}


/***********************************************************************
 * tioFrame()
 *
 * Draw the box frame around the specified rectangle
 **********************************************************************/
void tioFrame( int top, int left, int rows, int cols, char type, char attr )
{
	int x, y;

	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left);

	// draw the top of the frame
	*vidmem++ = Frames[type][0];
	*vidmem++ = attr;
	for ( x = 0; x < cols - 2; x++ )
	{
		*vidmem++ = Frames[type][1];
		*vidmem++ = attr;
	}
	*vidmem++ = Frames[type][2];
	*vidmem++ = attr;

	// draw the left and right edges
	vidmem += tioStride - cols * 2;
	for ( y = 0; y < rows - 2; y++ )
	{
		*vidmem++ = Frames[type][3];
		*vidmem++ = attr;
		vidmem += (cols - 2) * 2;
		*vidmem++ = Frames[type][5];
		*vidmem++ = attr;
		vidmem += tioStride - cols * 2;
	}

	// draw the bottom of the frame
	*vidmem++ = Frames[type][6];
	*vidmem++ = attr;
	for ( x = 0; x < cols - 2; x++ )
	{
		*vidmem++ = Frames[type][7];
		*vidmem++ = attr;
	}
	*vidmem++ = Frames[type][8];
	*vidmem++ = attr;
}


/***********************************************************************
 * tioLeftString()
 *
 * Draw the string left justified at the specified position
 **********************************************************************/
void tioLeftString( int row, int col, char *s, char attr )
{
	WriteString(window.top + row, window.left + col, s, attr);
}


/***********************************************************************
 * tioCenterString()
 *
 * Draw the string centered at the specified position
 **********************************************************************/
void tioCenterString( int row, int col, char *s, char attr )
{
	col -= strlen(s) / 2;
	WriteString(window.top + row, window.left + col, s, attr);
}


/***********************************************************************
 * tioRightString()
 *
 * Draw the string right justified at the specified position
 **********************************************************************/
void tioRightString( int row, int col, char *s, char attr )
{
	col -= strlen(s) - 1;
	WriteString(window.top + row, window.left + col, s, attr);
}


/***********************************************************************
 * tioLeftString()
 *
 * Draw the string left justified within the specified row region
 **********************************************************************/
void tioLeftString( int row, int left, int right, char *s, char attr )
{
	char buffer[256];
	int n = strlen(s);

	memset(buffer, ' ', tioScreenCols);
	buffer[right - left + 1] = '\0';

	if ( n > right - left + 1 )
		memcpy(buffer, s, right - left + 1);
	else
		memcpy(buffer, s, n);

	WriteString(window.top + row, window.left + left, buffer, attr);
}


/***********************************************************************
 * tioCenterString()
 *
 * Draw the string centered within the specified row region
 **********************************************************************/
void tioCenterString( int row, int left, int right, char *s, char attr )
{
	char buffer[256];
	int n;

	memset(buffer, ' ', tioScreenCols);
	buffer[right - left + 1] = '\0';

	n = (right - left + 1 - strlen(s)) / 2;

	if (n < 0)
		memcpy(buffer, &s[-n], right - left + 1);
	else
		memcpy(&buffer[n], s, strlen(s));

	WriteString(window.top + row, window.left + left, buffer, attr);
}


/***********************************************************************
 * tioRightString()
 *
 * Draw the string right justified within the specified row region
 **********************************************************************/
void tioRightString( int row, int left, int right, char *s, char attr )
{
	char buffer[256];
	int n;

	memset(buffer, ' ', tioScreenCols);
	buffer[right - left + 1] = '\0';

	n = (right - left + 1 - strlen(s));

	if (n < 0)
		memcpy(buffer, &s[-n], right - left + 1);
	else
		memcpy(&buffer[n], s, strlen(s));

	WriteString(window.top + row, window.left + left, buffer, attr);
}


/***********************************************************************
 * tioWindow()
 **********************************************************************/
void tioWindow( int top, int left, int rows, int cols )
{
	window.top = top;
	window.left = left;
	window.rows = rows;
	window.cols = cols;
}


/***********************************************************************
 * tioSetPos()
 **********************************************************************/
void tioSetPos(int row, int col)
{
	cursor.row = row;
	cursor.col = col;

	// set bios cursor position
	SetCursorPos(window.top + row, window.left + col);
}


/***********************************************************************
 * tioClearWindow()
 **********************************************************************/
void tioClearWindow( void )
{
	Scroll(0, nAttribute, window.top, window.left, window.top + window.rows - 1, window.left + window.cols - 1);
	tioSetPos(0, 0);
}


/***********************************************************************
 * tioSetAttribute()
 *
 * Set the default attribute used for print style functions
 **********************************************************************/
char tioSetAttribute( char attr )
{
	char old = nAttribute;
	nAttribute = attr;
	return old;
}


/***********************************************************************
 * Display a message using standard printf formatting
 **********************************************************************/
void tioPrint( char * fmt, ... )
{
	char msg[256];
	va_list argptr;
	va_start( argptr, fmt );
	vsprintf( msg, fmt, argptr );
	va_end(argptr);
	WriteString(window.top + cursor.row, window.left + cursor.col, msg, nAttribute);
	cursor.col = 0;
	if ( ++cursor.row >= window.rows )
	{
		Scroll(1, nAttribute, window.top, window.left, window.top + window.rows - 1, window.left + window.cols - 1);
		--cursor.row;
	}
	tioSetPos(cursor.row, cursor.col);
}


int tioGauge(int n, int total)
{
	static char gauge[kGaugeLen + 1];
	char buffer[128];
	int nGauge, p100, nFrac;

	nGauge = n * kGaugeLen / total;
	p100 = n * 100 / total;
	nFrac = n * kGaugeLen * 2 / total & 0x01;

	memset(gauge, '�', kGaugeLen);
	memset(gauge, '�', nGauge);
	gauge[nGauge] = "��"[nFrac];
	gauge[kGaugeLen] = '\0';

	sprintf(buffer, "%s  %3d%%", gauge, p100);
	WriteString(window.top + cursor.row, window.left + cursor.col, buffer, nAttribute);

	if (n < total)
		return 1;

	tioPrint("");
	return 0;
}

void tioSaveWindow( char *b, int top, int left, int rows, int cols )
{
	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left);

	for( int y = 0; y < rows; y++ )
	{
		memcpy(b, vidmem, cols * 2);
		b += cols * 2;
		vidmem += tioStride;
	}
}


void tioRestoreWindow( char *b, int top, int left, int rows, int cols )
{
	rows = ClipHigh(rows, window.rows);
	cols = ClipHigh(cols, window.cols);
	char *vidmem = VideoAddress(window.top + top, window.left + left);

	for( int y = 0; y < rows; y++ )
	{
		memcpy(vidmem, b, cols * 2);
		b += cols * 2;
		vidmem += tioStride;
	}
}


