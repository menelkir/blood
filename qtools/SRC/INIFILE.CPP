/*******************************************************************************
	FILE:			INIFILE.CPP

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#include <io.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "typedefs.h"
#include "inifile.h"
#include "debug4g.h"


IniFile::IniFile( char *sFile )
{
	head.next = &head;
	strcpy(filename, sFile);
	Load();
}


IniFile::Load( void )
{
	int nLineLen;
	char lineBuf[256];
	char *sBuffer, *pStart, *pEqual;
	FILE *fIn;
	char *p;

	curNode = &head;

	fIn = fopen(filename, "rt");

	if ( fIn != NULL )
	{
		// build the list node by node
		pStart = sBuffer;

		while ( fgets(lineBuf, 256, fIn) != NULL )
		{
			if ((p = strchr(lineBuf,'\n')) != NULL)
				*p = '\0';

			pStart = lineBuf;

			// ignore leading spaces
			while (isspace(*pStart))
				pStart++;

			nLineLen = strlen(pStart);

			curNode->next = (NODE *)malloc(sizeof(NODE) + nLineLen);
			dassert(curNode->next != NULL);

			prevNode = curNode;
			curNode = curNode->next;

			strcpy(&curNode->line[0], pStart);

			switch ( *pStart )
			{
				case '\0':
				case ';':
					break;

				case '[':
					if ( strchr(pStart, ']') == NULL )
					{
						dprintf("Error in INI section header: \"%s\"\n", curNode->line);
						free(curNode);
						curNode = prevNode;
						continue;
					}
					break;

				default:
					pEqual = strchr(pStart, '=');
					if ( pEqual <= pStart )
					{
						dprintf("Error in INI file: \"%s\"\n", pStart);
						free(curNode);
						curNode = prevNode;
					}
					break;
			}
		}
	}
	// mark the end of the list
	curNode->next = &head;
}


void IniFile::Save( void )
{
	int hFile;
	char sLine[256];

	hFile = open(filename, O_CREAT | O_WRONLY | O_TEXT | O_TRUNC, S_IREAD | S_IWRITE);
	dassert(hFile != -1);

	curNode = head.next;

	while ( curNode != &head )
	{
		sprintf(sLine, "%s\n", curNode->line);
		write(hFile, sLine, strlen(sLine));
		curNode = curNode->next;
	}
	close(hFile);
}


BOOL IniFile::FindSection( char *sSection )
{
	char sLine[256];

	curNode = prevNode = &head;

	if ( sSection == NULL )
		return TRUE;

	sprintf(sLine, "[%s]", sSection);

	do
	{
		prevNode = curNode;
		curNode = curNode->next;
		if ( curNode == &head)
			return FALSE;
	}
	while (stricmp(curNode->line, sLine) != 0);

	return TRUE;
}


BOOL IniFile::FindKey( char *sKey )
{
	// skip past section header
	prevNode = curNode;
	curNode = curNode->next;

	while (curNode != &head)
	{
		// ignore comments and blank lines
		if (curNode->line[0] == ';' || curNode->line[0] == '\0')
		{
			prevNode = curNode;
			curNode = curNode->next;
			continue;
		}

		// stop if we hit another section header
		if (curNode->line[0] == '[')
			return FALSE;

		char *pEqual = strchr(curNode->line, '=');
		dassert(pEqual != NULL);

		char *pEnd = pEqual;
		while ( isspace(*(pEnd - 1)) )
			pEnd--;

		// temporarily mark the the end of the string
		char cTemp = *pEnd;
		*pEnd = '\0';
		if (stricmp(sKey, curNode->line) == 0)
		{
			// restore the marker
			*pEnd = cTemp;
			keyValue = pEqual + 1;

			while ( isspace(*keyValue) )
				keyValue++;

			return TRUE;
		}
		*pEnd = cTemp;

		prevNode = curNode;
		curNode = curNode->next;
	}
	return FALSE;
}


void IniFile::AddSection( char *sSection )
{
	NODE *newNode;
	char sLine[256];

	// first insert a blank line
	if ( prevNode != &head )
	{
		newNode = (NODE *)malloc(sizeof(NODE));
		dassert(newNode != NULL);

		newNode->line[0] = '\0';

		// insert the new node into the linked list
		newNode->next = prevNode->next;
		prevNode->next = newNode;
		prevNode = newNode;
	}

	sprintf(sLine, "[%s]", sSection);
	newNode = (NODE *)malloc(sizeof(NODE) + strlen(sLine));
	dassert(newNode != NULL);

	strcpy(&newNode->line[0], sLine);

	// insert the new node into the linked list
	newNode->next = prevNode->next;
	prevNode->next = newNode;
	prevNode = newNode;
}


void IniFile::AddKeyString( char *sKey, char *sValue )
{
	NODE *newNode;
	char sLine[256];

	sprintf(sLine, "%s=%s", sKey, sValue);
	newNode = (NODE *)malloc(sizeof(NODE) + strlen(sLine));
	dassert(newNode != NULL);

	strcpy(&newNode->line[0], sLine);

	// insert the new node into the linked list
	newNode->next = prevNode->next;
	prevNode->next = newNode;
	curNode = newNode;
}


void IniFile::ChangeKeyString( char *sKey, char *sValue )
{
	NODE *newNode;
	char sLine[256];

	sprintf(sLine, "%s=%s", sKey, sValue);
	newNode = (NODE *)realloc(curNode, sizeof(NODE) + strlen(sLine));
	dassert(newNode != NULL);

	strcpy(&newNode->line[0], sLine);

	// block may have moved, so set prev->next pointer
	prevNode->next = newNode;
}


/***********************************************************************
 * iniKeyExists()
 **********************************************************************/
BOOL IniFile::KeyExists( char *sSection, char *sKey )
{
	if ( FindSection(sSection) && FindKey(sKey) )
		return TRUE;

	return FALSE;
}


/***********************************************************************
 * iniSectionExists()
 **********************************************************************/
BOOL IniFile::SectionExists( char *sSection )
{
	return FindSection(sSection);
}


/***********************************************************************
 * iniPutKeyString()
 **********************************************************************/
void IniFile::PutKeyString( char *sSection, char *sKey, char *sValue )
{
	if ( FindSection(sSection) )
	{
		if ( FindKey(sKey) )
			ChangeKeyString(sKey, sValue);
		else
			AddKeyString(sKey, sValue);
	}
	else
	{
		AddSection(sSection);
		AddKeyString(sKey, sValue);
	}
}


/***********************************************************************
 * iniGetKeyString()
 **********************************************************************/
char *IniFile::GetKeyString( char *sSection, char *sKey, char *sDefault )
{
	if ( FindSection(sSection) && FindKey(sKey) )
		return keyValue;

	return sDefault;
}


/***********************************************************************
 * iniPutKeyInt()
 **********************************************************************/
void IniFile::PutKeyInt( char *sSection, char *sKey, int nValue )
{
	char sValue[256];

	itoa(nValue, sValue, 10);

	PutKeyString(sSection, sKey, sValue);
}


/***********************************************************************
 * iniGetKeyInt()
 **********************************************************************/
int IniFile::GetKeyInt( char *sSection, char *sKey, int nDefault )
{
	if ( FindSection(sSection) && FindKey(sKey) )
		return strtol(keyValue, NULL, 0);

	return nDefault;
}


/***********************************************************************
 * iniGetKeyBool()
 **********************************************************************/
BOOL IniFile::GetKeyBool( char *sSection, char *sKey, int nDefault )
{
	return (BOOL)GetKeyInt(sSection, sKey, nDefault);
}


/***********************************************************************
 * iniPutKeyHex()
 **********************************************************************/
void IniFile::PutKeyHex( char *sSection, char *sKey, int nValue )
{
	char sValue[256] = "0x";

	itoa(nValue, &sValue[2], 16);

	PutKeyString(sSection, sKey, sValue);
}



/***********************************************************************
 * iniGetKeyHex()
 **********************************************************************/
int IniFile::GetKeyHex( char *sSection, char *sKey, int nDefault )
{
	return GetKeyInt(sSection, sKey, nDefault);
}




/***********************************************************************
 * iniRemoveKey()
 **********************************************************************/
void IniFile::RemoveKey( char *sSection, char *sKey )
{
	if ( FindSection(sSection) )
	{
		if ( FindKey(sKey) )
		{
			prevNode->next = curNode->next;
			free(curNode);
			curNode = prevNode->next;
		}
	}
}


/***********************************************************************
 * iniRemoveSection()
 **********************************************************************/
void IniFile::RemoveSection( char *sSection )
{
	if ( FindSection(sSection) )
	{
		prevNode = curNode;
		curNode = curNode->next;

		while (curNode != &head && curNode->line[0] != '[')
		{
			prevNode->next = curNode->next;
			free(curNode);
			curNode = prevNode->next;
		}
	}
}


IniFile::~IniFile( void )
{
	curNode = head.next;

	while (curNode != &head)
	{
		prevNode = curNode;
		curNode = prevNode->next;
		free(prevNode);
	}
}

