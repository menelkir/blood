/*******************************************************************************
	FILE:			GRABQBM.CPP

	DESCRIPTION:	Import an RLE bitmap from a graphic file

	AUTHOR:			Peter M. Freese
	CREATED:		09-20-95
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <helix.h>
#include <string.h>
#include <dos.h>
#include <io.h>
#include <fcntl.h>
#include <conio.h>

#include "typedefs.h"
#include "key.h"
#include "misc.h"
#include "debug4g.h"
#include "getopt.h"
#include "pcx.h"
#include "lbm.h"
#include "mouse.h"
#include "timer.h"
#include "gfx.h"

enum FILE_TYPE
{
	FT_NONE,
	FT_PAL,
	FT_PCX,
	FT_LBM,
};

static struct
{
	char *ext;
	FILE_TYPE fileType;
} ftTable[] =
{
	{ ".LBM", FT_LBM },
	{ ".PAL", FT_PAL },
	{ ".PCX", FT_PCX },
	{ ".PCC", FT_PCX },
};


char srcName[_MAX_PATH] = "";

volatile int gGameClock = 0;
long gFrameClock = 0, gFrameTicks;
int gFileNumber = 0;
int x0 = 0, y0 = 0, x1 = 0, y1 = 0;
int bitModel = BM_TRAW;


void ClockStrobe( void )
{
	gGameClock++;
}

void CLOCK_STROBE_END( void ) {};

void LockClockStrobe( void )
{
	dpmiLockMemory(FP_OFF(&ClockStrobe), FP_OFF(&CLOCK_STROBE_END) - FP_OFF(&ClockStrobe));
	dpmiLockMemory(FP_OFF(&gGameClock), sizeof(long));
}

void UnlockClockStrobe( void )
{
	dpmiLockMemory(FP_OFF(&ClockStrobe), FP_OFF(&CLOCK_STROBE_END) - FP_OFF(&ClockStrobe));
	dpmiLockMemory(FP_OFF(&gGameClock), sizeof(long));
}


/***********************************************************************
 * ShowUsage
 *
 * Display command-line parameter usage, then exit.
 **********************************************************************/
void ShowUsage(void)
{
	printf("Syntax:  GRABQBM [options] from\n");
	printf("-r       RLE masked\n");
	printf("-t       Transparent raw (default)\n");
	printf("-w       raW\n");
	printf("-?       This help\n");
	exit(0);
}


void QuitMessage(char * fmt, ...)
{
	char msg[80];
	va_list argptr;
	va_start( argptr, fmt );
	vsprintf( msg, fmt, argptr );
	va_end(argptr);
	printf(msg);
	exit(1);
}


PALETTE palette;
QBITMAP *qbmSource, *qbmSelection;


FILE_TYPE GetFileType( char *filename )
{
	char ext[_MAX_EXT];
	_splitpath(filename, NULL, NULL, NULL, ext);

	for ( int i = 0; i < LENGTH(ftTable); i++ )
	{
		if ( stricmp(ext, ftTable[i].ext) == 0 )
			return ftTable[i].fileType;
	}

	return FT_NONE;
}


void LoadSource( void )
{
	int nResult;
	int srcWidth = 0, srcHeight = 0;

	FILE_TYPE fileType = GetFileType(srcName);

	if ( fileType == FT_NONE )
		QuitMessage("Don't know how to handle file %s", srcName);

	// determine the size of the source image
	switch ( fileType )
	{
		case FT_LBM:
			nResult = ReadLBM(srcName, NULL, &srcWidth, &srcHeight, NULL);
			break;

		case FT_PCX:
			nResult = ReadPCX(srcName, NULL, &srcWidth, &srcHeight, NULL);
			break;
	}

	if (nResult != 0)
		QuitMessage("Problem reading %s: error=%d", srcName, nResult);

	qbmSource = (QBITMAP *)malloc(sizeof(QBITMAP) + srcWidth * srcHeight);
	dassert(qbmSource != NULL);

	BYTE *pSourceBits = qbmSource->data;

	// decode the image
	switch ( fileType )
	{
		case FT_LBM:
			nResult = ReadLBM(srcName, palette, &srcWidth, &srcHeight, &pSourceBits);
			break;

		case FT_PCX:
			nResult = ReadPCX(srcName, palette, &srcWidth, &srcHeight, &pSourceBits);
			break;
	}


	qbmSource->bitModel = BM_RAW;
	qbmSource->tcolor = 0;
	qbmSource->cols = (short)srcWidth;
	qbmSource->rows = (short)srcHeight;
	qbmSource->stride = (short)srcWidth;
}


void DrawMouse( int x, int y )
{
	gfxHLineROP(y, x - 4, x - 2);
	gfxHLineROP(y, x + 2, x + 4);
	gfxVLineROP(x, y - 4, y - 2);
	gfxVLineROP(x, y + 2, y + 4);
}


void DrawSelect( void )
{
	if (x0 < x1 && y0 < y1)
	{
		gfxHLineROP(y0, x0, x1);
		gfxHLineROP(y1, x0, x1);
		if ( y0 + 1 < y1 - 1)
		{
			gfxVLineROP(x0, y0 + 1, y1 - 1);
			gfxVLineROP(x1, y0 + 1, y1 - 1);
		}
	}
}


void TryOut( void )
{
	int oldX = Mouse::X, oldY = Mouse::Y;
	BYTE nbuttons;
	static BYTE obuttons = 0;

	DrawMouse(oldX, oldY);

	while (1)
	{
		gFrameTicks = gGameClock - gFrameClock;
		gFrameClock += gFrameTicks;

		Mouse::Read(gFrameTicks);

		// which buttons just got pressed?
		nbuttons = (BYTE)(~obuttons & Mouse::buttons);

		if (kbhit() )
		{
			int k = getch();
			if ( k == 27 )
				break;
		}

		if (Mouse::buttons & 1)
		{
			DrawMouse(oldX, oldY);
			gfxDrawBitmap(qbmSelection, oldX, oldY);
			DrawMouse(oldX, oldY);
		}

		if ( Mouse::dX || Mouse::dY )
		{
			DrawMouse(oldX, oldY);
			oldX = Mouse::X;
			oldY = Mouse::Y;
			DrawMouse(oldX, oldY);
		}

		obuttons = Mouse::buttons;
	}

	DrawMouse(oldX, oldY);
}


void ShrinkSelection( void )
{
	int stride = qbmSource->stride;
	BYTE tcolor = qbmSource->data[y0 * stride + x0];
	BYTE *pBits;
	int i;

	while (y0 < y1)
	{
		pBits = &qbmSource->data[y0 * stride + x0];
		for (i = x0; i <= x1; i++, pBits++)
		{
			if ( *pBits != tcolor )
				break;
		}
		if ( i <= x1 )
			break;

		DrawSelect();		// erase old selection
		y0++;
		DrawSelect();		// draw new selection
	}
	DrawSelect();		// erase old selection
	y0--;
	DrawSelect();		// draw new selection

	while (y0 < y1)
	{
		pBits = &qbmSource->data[y1 * stride + x0];
		for (i = x0; i <= x1; i++, pBits++)
		{
			if ( *pBits != tcolor )
				break;
		}
		if ( i <= x1 )
			break;

		DrawSelect();		// erase old selection
		y1--;
		DrawSelect();		// draw new selection
	}
	DrawSelect();		// erase old selection
	y1++;
	DrawSelect();		// draw new selection

	while (x0 < x1)
	{
		pBits = &qbmSource->data[y0 * stride + x0];
		for (i = y0; i <= y1; i++, pBits += stride)
		{
			if ( *pBits != tcolor )
				break;
		}
		if ( i <= y1 )
			break;

		DrawSelect();		// erase old selection
		x0++;
		DrawSelect();		// draw new selection
	}
	DrawSelect();		// erase old selection
	x0--;
	DrawSelect();		// draw new selection

	while (x0 < x1)
	{
		pBits = &qbmSource->data[y0 * stride + x1];
		for (i = y0; i <= y1; i++, pBits += stride)
		{
			if ( *pBits != tcolor )
				break;
		}
		if ( i <= y1 )
			break;

		DrawSelect();		// erase old selection
		x1--;
		DrawSelect();		// draw new selection
	}
	DrawSelect();		// erase old selection
	x1++;
	DrawSelect();		// draw new selection
}


void WriteSelection( int size )
{
	char qbmName[_MAX_PATH];
	sprintf(qbmName, "GRAB%02d.QBM", gFileNumber++);
	if ( !FileSave(qbmName, qbmSelection, sizeof(QBITMAP) + size) )
		QuitMessage("Unable to save file %s", qbmName);
}


void CompressSelection( void )
{
	int cols = x1 - x0 - 1;
	int rows = y1 - y0 - 1;
	int stride = qbmSource->stride;
	int sourceSkip = stride - cols;
	BYTE tcolor = qbmSource->data[y0 * stride + x0];

	// allocate a worst case buffer
	BYTE *buffer = (BYTE *)malloc(rows * (cols * 3 + 2));
	dassert(buffer != NULL);
	BYTE *pEncode = buffer;

	BYTE *pBitmap = &qbmSource->data[(y0 + 1) * stride + x0 + 1];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; )
		{
			int skip, run;

			for (skip = 0; j < cols && *pBitmap == tcolor; j++, skip++, pBitmap++);
			*pEncode++ = (BYTE)skip;

			BYTE *pStart = pEncode++;
			for (run = 0; j < cols && *pBitmap != tcolor; j++, run++, *pEncode++ = *pBitmap++);
			*pStart = (BYTE)run;

			if ( j == cols && run > 0 )
			{
				*pEncode++ = 0;
				*pEncode++ = 0;
			}
		}
		pBitmap += sourceSkip;
	}

	int size = pEncode - buffer;

	qbmSelection = (QBITMAP *)malloc(sizeof(QBITMAP) + size);
	dassert(qbmSelection != NULL);

	memcpy(qbmSelection->data, buffer, size);
	free(buffer);

	qbmSelection->bitModel = (BYTE)bitModel;
	qbmSelection->tcolor = tcolor;
	qbmSelection->cols = (short)cols;
	qbmSelection->rows = (short)rows;
	qbmSelection->stride = (short)cols;

	WriteSelection(size);
}


void CopySelection( void )
{
	int cols = x1 - x0 - 1;
	int rows = y1 - y0 - 1;
	int stride = qbmSource->stride;
	BYTE tcolor = qbmSource->data[y0 * stride + x0];

	int size = rows * cols;
	qbmSelection = (QBITMAP *)malloc(sizeof(QBITMAP) + size);
	dassert(qbmSelection != NULL);

	BYTE *pDest = qbmSelection->data;
	BYTE *pSource = &qbmSource->data[(y0 + 1) * stride + x0 + 1];

	for (int i = 0; i < rows; i++)
	{
		memcpy(pDest, pSource, cols);
		pDest += cols;
		pSource += stride;
	}

	qbmSelection->bitModel = (BYTE)bitModel;
	qbmSelection->tcolor = tcolor;
	qbmSelection->cols = (short)cols;
	qbmSelection->rows = (short)rows;
	qbmSelection->stride = (short)cols;

	WriteSelection(size);
}


void Select( void )
{
	int oldX = Mouse::X, oldY = Mouse::Y;
	BYTE nbuttons;
	static BYTE obuttons = 0;

	timerInstall();
	LockClockStrobe();
	timerRegisterClient(ClockStrobe, 120);

	mouseInit();
	gfxDrawBitmap(qbmSource, 0, 0);

	gROP = ROP_XOR;
	Video.SetColor(255);
	DrawMouse(oldX, oldY);

	while (1)
	{
		gFrameTicks = gGameClock - gFrameClock;
		gFrameClock += gFrameTicks;

		if (kbhit() )
		{
			int k = getch();
			if ( k == 27 )
				break;

			switch ( k )
			{
				case 13:
					DrawMouse(oldX, oldY);	// erase old mouse
					if ( bitModel != BM_RAW )
						ShrinkSelection();

					switch ( bitModel )
					{
						case BM_RAW:
						case BM_TRAW:
							CopySelection();
							break;

						case BM_RLE:
							CompressSelection();
							break;
					}


					TryOut();

					free(qbmSelection);
					qbmSelection = NULL;
					x0 = y0 = x1 = y1 = 0;

					gfxDrawBitmap(qbmSource, 0, 0);
					DrawMouse(oldX, oldY);	// redraw old mouse
					break;
			}
		}

		BOOL bInside = Mouse::X > x0 && Mouse::X < x1 && Mouse::Y > y0 && Mouse::Y < y1;

		Mouse::Read(gFrameTicks);

		// which buttons just got pressed?
		nbuttons = (BYTE)(~obuttons & Mouse::buttons);

		// drag selection rectange with right mouse button
		if ( bInside && (Mouse::buttons & 2) && (Mouse::dX || Mouse::dY) )
		{
			DrawSelect();		// erase old selection
			x0 += Mouse::dX;
			x1 += Mouse::dX;
			y0 += Mouse::dY;
			y1 += Mouse::dY;
			DrawSelect();		// draw new selection
		}

		if (nbuttons & 1)
		{
			DrawMouse(oldX, oldY);	// erase old mouse

			DrawSelect();		// erase old selection

			x0 = x1 = Mouse::X;
			y0 = y1 = Mouse::Y;
		}
		else if (obuttons & 1)
		{
			if (Mouse::buttons & 1)
			{
				if ( Mouse::dX || Mouse::dY )
				{
					DrawSelect();		// erase old selection
					x1 = Mouse::X;
					y1 = Mouse::Y;
					DrawSelect();		// draw new selection
				}
			}
			else
			{
				// restore mouse cursor
				DrawMouse(Mouse::X, Mouse::Y);	// draw new mouse
				oldX = Mouse::X;
				oldY = Mouse::Y;
			}
		}
		else if ( Mouse::dX || Mouse::dY )
		{
			DrawMouse(Mouse::X, Mouse::Y);	// draw new mouse
			DrawMouse(oldX, oldY);	// erase old mouse
			oldX = Mouse::X;
			oldY = Mouse::Y;
		}

		obuttons = Mouse::buttons;
	}

	DrawMouse(oldX, oldY);

	timerRemoveClient(ClockStrobe);
	UnlockClockStrobe();
	timerRemove();
}


/***********************************************************************
 * Process command line arguments
 **********************************************************************/
void ParseOptions( void )
{
	enum {
		kSwitchHelp,
		kSwitchRLE,
		kSwitchTRaw,
		kSwitchRaw,
	};
	static SWITCH switches[] = {
		{ "?", kSwitchHelp, 0 },
		{ "R", kSwitchRLE, 0 },
		{ "T", kSwitchTRaw, 0 },
		{ "W", kSwitchRaw, 0 },
	};
	int r;
	while ( (r = GetOptions(switches)) != GO_EOF ) {
		switch (r)
		{
			case GO_INVALID:
				QuitMessage("Invalid argument: %s", OptFull);

			case GO_FULL:
				if ( srcName[0] == '\0' )
					strcpy(srcName, OptFull);
				else
					QuitMessage("Unexpected argument: %s", OptFull);
				break;

			case kSwitchHelp:
				ShowUsage();
				break;

			case kSwitchRLE:
				bitModel = BM_RLE;
				break;

			case kSwitchTRaw:
				bitModel = BM_TRAW;
				break;

			case kSwitchRaw:
				bitModel = BM_RAW;
				break;
		}
	}
}


void main( int argc )
{
	if (argc < 2)
		ShowUsage();

	ParseOptions();

	if ( srcName[0] == '\0' )
		ShowUsage();

	AddExtension(srcName, ".PCX");

	LoadSource();

	if ( !mouseInit() )
		QuitMessage("Mouse not detected");

	int oldMode = gGetMode();

	if (!gFindMode(320, 200, 256, CHAIN256))
		QuitMessage("Helix driver not found");

	Video.SetMode();
 	gSetDACRange(0, 256, palette);

	gfxSetClip(0, 0, 320, 200);

	Select();

	gSetMode(oldMode);

}
