/*******************************************************************************
	FILE:			3DBRAND.CPP

	DESCRIPTION:	Special branding utility for 3D Realms

	AUTHOR:			Peter M. Freese
	CREATED:		06-30-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <io.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <dos.h>

#include "typedefs.h"
#include "encrypt.h"
#include "crc32.h"
#include "misc.h"
#include "protect.h"

#define kEncryptKey	'PMF1'

void QuitMessage(char * fmt, ...)
{
	char msg[80];
	va_list argptr;
	va_start( argptr, fmt );
	vsprintf( msg, fmt, argptr );
	va_end(argptr);
	printf(msg);
	exit(1);
}


/***********************************************************************
 * ShowUsage
 *
 * Display command-line parameter usage, then exit.
 **********************************************************************/
void ShowUsage(void)
{
	printf("Q Studios demo branding utility -- licensed exclusively to 3D Realms\n");
	printf("Usage:\n");
	printf("  3DBRAND \"Name\" [key file]    create a demo brand\n");
	exit(0);
}

#define kDays	30

void CreateBrand( char *name, char *filename )
{
	if ( strlen(name) == 0 )
		QuitMessage("Name cannot be blank");

	gBrand = (BRAND *)malloc(sizeof(BRAND) + 128);
	gBrand->expire = time(NULL) + kDays * 24 * 60 * 60;
	gBrand->type = kBrandDemo;
	strcpy(gBrand->name, "3D Realms: ");
	strcat(gBrand->name, name);

	int size = sizeof(BRAND) + strlen(gBrand->name) + 1;
	gBrand->crc = CRC32(&gBrand->expire, size - 4);
	cryptEncode(gBrand, size, kEncryptKey);

	if ( !FileSave(filename, gBrand, size) )
		QuitMessage("Error creating brand file %s", filename);

	printf("%s created\n", filename);
	exit(0);
}


void main( int argc, char *argv[] )
{
	switch ( argc )
	{
		case 2:
			CreateBrand(argv[1], "VGA.DAT");
			break;

		case 3:
			CreateBrand(argv[1], argv[2]);
			break;

		default:
			ShowUsage();
			break;
	}
}















