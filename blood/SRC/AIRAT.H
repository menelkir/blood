/*******************************************************************************
	FILE:			AIRAT.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __AIRAT_H
#define __AIRAT_H

#include "ai.h"


extern AISTATE ratIdle;
extern AISTATE ratSearch;
extern AISTATE ratChase;
extern AISTATE ratDodge;
extern AISTATE ratRecoil;
extern AISTATE ratGoto;
extern AISTATE ratBite;


#endif	// __AIRAT_H
