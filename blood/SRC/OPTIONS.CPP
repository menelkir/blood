/*******************************************************************************
	FILE:			OPTIONS.CPP

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#include <fcntl.h>
#include <io.h>
#include <sys\stat.h>

#include "debug4g.h"
#include "engine.h"
#include "error.h"
#include "gfx.h"
#include "globals.h"
#include "helix.h"
#include "key.h"
#include "resource.h"
#include "trig.h"
#include "config.h"

void optLoadINI( void )
{
	// load the play options
	gDetail			= BloodINI.GetKeyInt("Options", "Detail", gDetail);

	gViewInterpolate = BloodINI.GetKeyBool("Options", "Interpolation",  gViewInterpolate);
	gViewHBobbing	= BloodINI.GetKeyBool("Options", "ViewHBobbing",    gViewHBobbing);
	gViewVBobbing	= BloodINI.GetKeyBool("Options", "ViewVBobbing",    gViewVBobbing);
	gRunBackwards   = BloodINI.GetKeyBool("Options", "RunBackwards",   	gRunBackwards);
	gFriendlyFire   = BloodINI.GetKeyBool("Options", "FriendlyFire",   	gFriendlyFire);
	gOverlayMap     = BloodINI.GetKeyBool("Options", "OverlayMap",     	gOverlayMap);
	gRotateMap      = BloodINI.GetKeyBool("Options", "RotateMap",      	gRotateMap);
	gGraphNumbers   = BloodINI.GetKeyBool("Options", "GraphNumbers",   	gGraphNumbers);
	gAutoAim		= BloodINI.GetKeyBool("Options", "AutoAim",   		gAutoAim);
	gInfiniteAmmo	= BloodINI.GetKeyBool("Options", "InfiniteAmmo",   	gInfiniteAmmo);
	gAimReticle		= BloodINI.GetKeyBool("Options", "AimReticle",   	gAimReticle);
	gSuperGib		= BloodINI.GetKeyBool("Options", "SuperGib",   		gSuperGib);
}

void optSaveINI( void )
{
	// save the play options
	BloodINI.PutKeyInt("Options", "Detail", gDetail);

	BloodINI.PutKeyInt("Options", "Interpolation", gViewInterpolate);
	BloodINI.PutKeyInt("Options", "ViewHBobbing", gViewHBobbing);
	BloodINI.PutKeyInt("Options", "ViewVBobbing", gViewVBobbing);
	BloodINI.PutKeyInt("Options", "AutoAim", gAutoAim);
	BloodINI.PutKeyInt("Options", "InfiniteAmmo", gInfiniteAmmo);
	BloodINI.PutKeyInt("Options", "AimReticle", gAimReticle);
	BloodINI.PutKeyInt("Options", "SuperGib", gSuperGib);
}


