/*******************************************************************************
	FILE:			FX.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		03-03-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __FX_H
#define __FX_H

#include "engine.h"

enum FXTYPE {
	kFXNone = -1,
	kFXBloodSquib1,
	kFXBloodSquib2,
	kFXRicWater,
	kFXRicStone,
	kFXRicClay,
	kFXRicPuff,
	kFXWaterSplash,
	kFXFootSplashWater,
	kFXFootSplashLava,
	kFXSmoke,
	kFXGibSquib,
	kFXGibFire,
	kFXGibWood,
	kFXGibWood2,
	kFXShardClear,
	kFXShardBlue,
	kFXShardRed,
	kFXShardGreen,
	kFXShardOrange,
	kFXBubble1,
	kFXBubble2,
	kFXBubble3,
	kFXBubbles,
	kFXBloodDrop,
	kFXSpark,
	kFXRespawn,
	kFXDarkMetal,
	kFXIcicles,
	kFXFlameLick,
	kFXBloodSplat1,
	kFXBloodSplat2,
	kFXBloodPuddle,

/*
	kFXBloodSquib,
	kFXWaterSquib,
	kFXSparkSquib,
	kFXSmokeSquib,
	kFXWoodSquib,
	kFXPlantSquib,
	kFXDirtSquib,
	kFXSnowSquib,

	kFXGlassShards,
	kFXWoodShards,
	kFXMetalShards,

	kFXTommyCultist,
	kFXShotgunCultist,
	kFXAxeZombie,
	kFXFatZombie,
	kFXEarthZombie,
	kFXFleshGargoyle,
	kFXStoneGargoyle,
	kFXFleshStatue,
	kFXStoneStatue,
	kFXPhantasm,
	kFXHound,
	kFXHand,
	kFXBrownSpider,
	kFXRedSpider,
	kFXBlackSpider,
	kFXMotherSpider,
	kFXGillBeast,
	kFXEel,
	kFXBat,
	kFXRat,
	kFXGreenPod,
	kFXGreenTentacle,
	kFXFirePod,
	kFXFireTentacle,
	kFXMotherPod,
	kFXMotherTentacle,
	kFXCerberus,
	kFXCerberus2,
	kFXTchernobog,
	kFXRachel,
*/

	kFXMax,
};


void fxProcess( void );
SPRITE *fxSpawn( int nSector, int x, int y, int z, int nType );

#endif

