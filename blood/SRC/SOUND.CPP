/*******************************************************************************
	FILE:			SOUND.CPP

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#include <string.h>
#include <dos.h>

#include "sound.h"
#include "debug4g.h"
#include "error.h"
#include "misc.h"
#include "globals.h"
#include "music.h"
#include "fx_man.h"
#include "config.h"

#define kSampleRate		11110

#define kMaxChannels	32

static BOOL sndActive = FALSE;

RESHANDLE hSong;
BYTE *pSong = NULL;
int songSize;

struct SAMPLE2D
{
	int hVoice;
	RESHANDLE hResource;
};


Resource gSoundRes;

SAMPLE2D Channel[kMaxChannels];


SAMPLE2D *FindChannel( void )
{
	for (int i = kMaxChannels - 1; Channel[i].hResource != NULL > 0; i--)
	{
		if ( i < 0 )
			ThrowError("No free channel available for sample");
	}

	return &Channel[i];
}


/*---------------------------------------------------------------------
   Function: LoadMidi

   Loads the midi file from disk.
---------------------------------------------------------------------*/
void sndPlaySong( char *songName, BOOL loopflag )
{
	if ( MusicDevice == -1 )
		return;

	if ( pSong )
		sndStopSong();

	if ( strlen(songName) == 0 )
		return;

	hSong = gSysRes.Lookup(songName, "MID");
	if (hSong == NULL)
	{
		dprintf("Could not load song %s\n", songName);
		return;
	}

	songSize = gSysRes.Size(hSong);

	pSong = (BYTE *)gSysRes.Lock(hSong);
	dpmiLockMemory(FP_OFF(pSong), songSize);

	// reset the volume, in case we were fading
	MUSIC_SetVolume(MusicVolume);

	// start the song
	MUSIC_PlaySong(pSong, loopflag);
}


BOOL sndIsSongPlaying( void )
{
	if ( MusicDevice == -1 )
		return FALSE;

	return (BOOL)MUSIC_SongPlaying();
}


void sndFadeSong( int nMilliseconds )
{
	MUSIC_FadeVolume(0, nMilliseconds);
}


void sndSetMusicVolume( int nVolume )
{
	MusicVolume = nVolume;
	MUSIC_SetVolume(MusicVolume);
}


void sndSetFXVolume( int nVolume )
{
	FXVolume = nVolume;
	FX_SetVolume(FXVolume);
}


void sndStopSong( void )
{
	if ( MusicDevice == -1 )
		return;

	if ( pSong )
	{
		MUSIC_StopSong();
		dpmiUnlockMemory(FP_OFF(pSong), songSize);
		gSysRes.Unlock(hSong);
		pSong = NULL;
	}
}


static void SoundCallback( ulong id )
{
	int *phVoice = (int *)id;
	*phVoice = 0;
}


void sndStartSample( char *sampleName, int nVol, int nChannel )
{
	if ( FXDevice == -1 )
		return;

	if ( strlen(sampleName) == 0 )
		return;

	SAMPLE2D *pChannel;
	if (nChannel == -1)
		pChannel = FindChannel();
	else
		pChannel = &Channel[nChannel];

	// if this is a fixed channel with an active voice, kill the sound
	if ( pChannel->hVoice > 0 )
		FX_StopSound(pChannel->hVoice);

	pChannel->hResource = gSoundRes.Lookup(sampleName, "RAW");
	if (pChannel->hResource == NULL)
	{
		dprintf("Could not load sample %s\n", sampleName);
		return;
	}

	int sampleSize = gSoundRes.Size(pChannel->hResource);

	BYTE *pRaw = (BYTE *)gSoundRes.Lock(pChannel->hResource);

	pChannel->hVoice = FX_PlayRaw(
		pRaw, sampleSize, kSampleRate, 0, nVol, nVol, nVol, nVol,
		(ulong)&pChannel->hVoice);
}


void sndKillAllSounds( void )
{
	for (int i = 0; i < kMaxChannels; i++)
	{
		if ( Channel[i].hVoice > 0 )
			FX_StopSound(Channel[i].hVoice);

		if ( Channel[i].hResource != NULL )
		{
			gSoundRes.Unlock(Channel[i].hResource);
			Channel[i].hResource = NULL;
		}
	}
}


void sndProcess( void )
{
	for (int i = 0; i < kMaxChannels; i++)
	{
		if (Channel[i].hVoice <= 0 && Channel[i].hResource != NULL )
		{
			gSoundRes.Unlock(Channel[i].hResource);
			Channel[i].hResource = NULL;
		}
	}
}


void InitSoundDevice(void)
{
	int status;

	if ( FXDevice == -1 )
		return;

	// Do special Sound Blaster, AWE32 stuff
	if ( FXDevice == SoundBlaster || FXDevice == Awe32 )
	{
		int MaxVoices;
		int MaxBits;
		int MaxChannels;

		status = FX_SetupSoundBlaster(SBConfig, &MaxVoices, &MaxBits, &MaxChannels);
	}
	else
	{
		fx_device device;
		status = FX_SetupCard( FXDevice, &device );
	}

	if ( status != FX_Ok )
		ThrowError(FX_ErrorString(status));

	status = FX_Init( FXDevice, NumVoices, NumChannels, NumBits, MixRate );
	if ( status != FX_Ok )
		ThrowError(FX_ErrorString(status));

	FX_SetVolume(FXVolume);

	// Set up our fx callback so we can unlock sound resources
	FX_SetCallBack(SoundCallback);
}


void InitMusicDevice(void)
{
	int status;

	if ( MusicDevice == -1 )
		return;

	status = MUSIC_Init( MusicDevice, MidiPort);
	if ( status != MUSIC_Ok )
		ThrowError(MUSIC_ErrorString(status));

	RESHANDLE hTimbres = gSysRes.Lookup("GMTIMBRE", "TMB");
	if ( hTimbres != NULL )
		MUSIC_RegisterTimbreBank((BYTE *)gSysRes.Load(hTimbres));

	MUSIC_SetVolume(MusicVolume);
}


/***********************************************************************
 * Remove sound/music drivers
 **********************************************************************/
void sndTerm(void)
{
	// prevent this from shutting down more than once
	if ( sndActive )
	{
		sndStopSong();

		dprintf("MUSIC_Shutdown()\n");
		MUSIC_Shutdown();

		FX_Shutdown();

		sndActive = FALSE;
	}
}

/***********************************************************************
 * Initialize sound/music drivers
 **********************************************************************/
void sndInit(void)
{
	InitSoundDevice();
	InitMusicDevice();
	atexit(sndTerm);
	sndActive = TRUE;

	gSoundRes.Init("SOUNDS.RFF", "*.RAW");
}
