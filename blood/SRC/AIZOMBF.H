/*******************************************************************************
	FILE:			AIZOMBF.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __AIZOMBF_H
#define __AIZOMBF_H

#include "ai.h"


extern AISTATE zombieFIdle;
extern AISTATE zombieFChase;
extern AISTATE zombieFGoto;
extern AISTATE zombieFHack;
extern AISTATE zombieFPuke;
extern AISTATE zombieFThrow;
extern AISTATE zombieFSearch;
extern AISTATE zombieFRecoil;


#endif	// __AIZOMBF_H
