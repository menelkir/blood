/*******************************************************************************
	FILE:			BACKPACK.CPP

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#include "player.h"

enum {
};

struct PACK
{
	int type;
	int count;
}

void packInit( PLAYER *pPlayer )
{
	pPlayer->packItems = 0;
	pPlayer->packNext = 0;
}

void packAdd( PLAYER *pPlayer, int nType, int nCount )
{
	PACK *pack = pPlayer->pack;

	for ( int i=0; i < pPlayer->packNext; i++)
	{
		if (nType == pack[i].type)
		{
			switch(nType)
			{
				default:
					pack[i].count = ClipHigh( pack[i].count + nCount, 10);
					return;
			}
		}
	}
}
