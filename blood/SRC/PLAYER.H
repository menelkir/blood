/*******************************************************************************
	FILE:			PLAYER.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __PLAYER_H
#define __PLAYER_H

#include "engine.h"
#include "db.h"
#include "qav.h"
#include "globals.h"
#include "gameutil.h"
#include "actor.h"
#include "dude.h"


// global weapon type indices (see also weapon item types in db.h)
enum
{
	kWeaponNone			= 0,
	kWeaponPitchfork	= 1,
	kWeaponTNT			= 2,
	kWeaponShotgun		= 3,
	kWeaponTommy		= 4,
	kWeaponFlare		= 5,
	kWeaponSprayCan		= 6,
	kWeaponSpear		= 7,
	kWeaponVoodoo		= 8,
	kWeaponShadow 		= 9,	// change to Ecto gun
	kWeaponBeast		= 10,	// remove when beast mode working
	kWeaponMax
};

enum
{
	kOrderAboveWater = 0,
	kOrderBelowWater = 1,
	kOrderMax
};

// global ammo types (see all ammo item types in db.h)
enum
{
	kAmmoNone			= -1,
	kAmmoSprayCan,
	kAmmoTNTBundle,
	kAmmoTNTProximity,
	kAmmoTNTRemote,
	kAmmoShell,
	kAmmoBullet,
	kAmmoBulletAP,
	kAmmoFlare,
	kAmmoFlareSB,
	kAmmoVoodoo,
	kAmmoSpear,
	kAmmoSpearXP,
	kAmmoMax,
};


// global pack slot enumerations
#define kMaxPackTime    (kTimerRate * 5)	// on five seconds, then off
enum
{
	kPackDocBag = 0,
	kPackDiveSuit,
	kPackFireSuit,
	kPackSeerBall,
	kPackCatEyes,
	kPackClone,
	kPackJumpBoots,
	kPackGasMask,
	kPackCloak,
	kPackMaxSlots,
};


// view effects
enum
{
	kVEWeaponFlash,
	kVEBlindness,
	kVEShock,
	kVEDamage,
	kVEQuake,
	kVEPickup,
	kVEDrowning,
	kVETilt,	// leaning
	kVEDelirium,
	kVEMax,
};

enum
{
	kArmorBody,
	kArmorFire,
	kArmorSpirit,
	kArmorMax,
};


#define kMaxArmorPoints	(200 << 4)
#define kMaxAirTime		( 10 * kTimerRate )

#define kBloodlustMax	( 15 * kTimerRate )
#define kBloodlustBeast ( 10 * kTimerRate )
#define kBloodlustHuman 0

// look up/down constants
#define kHorizDefault   90
#define kLookMax		60
#define kHorizUpMax		120
#define kHorizDownMax	180

// distance in pixels for picking up items and pushing things
#define kTouchXYDist	48
#define kTouchZDist		32
#define kPushXYDist		64

// Life modes
#define kModeHuman		0
#define kModeBeast		1

#define	kMaxLackeys		4


/***********************************************************************
 * Global structures
 **********************************************************************/

struct PACKITEM
{
	BOOL	inUse;
	int		count;
};

union BUTTONFLAGS
{
	char			byte;
	struct
	{
		unsigned	run : 1;
		unsigned	jump : 1;
		unsigned	crouch : 1;
		unsigned	shoot : 1;
		unsigned	shoot2 : 1;
		unsigned	lookup : 1;
		unsigned	lookdown : 1;
	};
};

union KEYFLAGS
{
	short			word;
	struct
	{
		unsigned	action :		1;
		unsigned	lookcenter :	1;
		unsigned	beast :			1;
		unsigned	pause :			1;
		unsigned	restart	:		1;
		unsigned	previtem :		1;
		unsigned	nextitem :		1;
		unsigned	useitem :		1;
		unsigned	quit :			1;
		unsigned	holster :		1;
		unsigned	medicine :		1;
		unsigned	jumpboots :		1;
		unsigned	crystalball :	1;
	};
};


union SYNCFLAGS
{
	char			byte;
	struct
	{
		unsigned	buttonChange : 1;
		unsigned	keyChange : 1;
		unsigned	weaponChange : 1;
		unsigned	forwardChange : 1;
		unsigned	turnChange : 1;
		unsigned	strafeChange : 1;
	};
};


struct INPUT
{
//	SYNCFLAGS 	syncFlags;
	BUTTONFLAGS	buttonFlags;
	KEYFLAGS 	keyFlags;
	uchar		newWeapon;

	schar 		forward;
	schar 		strafe;
	sshort 		turn;
};


struct PROFILE
{
	int			skill;
	char		name[20];
};


extern PROFILE gProfile[kMaxPlayers];


struct PLAYER
{
	SPRITE		*sprite;
	XSPRITE		*xsprite;
	DUDEINFO	*pDudeInfo;
	INPUT		input;
	int			NPSTART;	// *** EVERYTHING FOLLOWING IS CHECKSUMMED ***
	int			nWeaponQAV;
	QAVCALLBACKID	weaponCallbackID;
	BOOL		run;
	int			moveState;
	int			moveDist;
	int			bobAmp, bobPhase, bobHeight, bobWidth;
	int			swayAmp, swayPhase, swayHeight, swayWidth;
	int			nPlayer;
	int			nSprite;
	int			lifeMode;			// beast or human
	int			bloodlust;
	int			viewZ;
	int			viewZVel;
	int			weaponZ;
	int			weaponZVel;
	int			look;
	int			horiz;
	int			horizOffset;
	int			slope;
	BOOL		diving;
	BOOL		hasKey[8];
	int         noDamage[kDamageMax];
	uchar		weapon;
	uchar		nextWeapon;
	int			weaponTimer;
	int			weaponState;
	int			weaponAmmo;		// indicate current ammo type in use
	BOOL		hasWeapon[kWeaponMax];
	int			weaponMode[kWeaponMax];
	int			weaponOrder[kOrderMax][kWeaponMax];
	int			ammoCount[kAmmoMax];
	BOOL		fLoopQAV;
	int			fuseTime;	// used for TNT callbacks
	int			throwTime;	// used for TNT callbacks
	int         throwPower;	// used for TNT callbacks
	VECTOR3D	aim;
	VECTOR3D	relAim;
	int			aimSprite;
	int			voodooTarget;
	int			deathTime;
	int			powerUpTimer[kMaxPowerUps];
	int			fragCount;
	int			fragInfo[kMaxPlayers];
	int			teamID;		// team the player is allied with in team modes, or player index in coop and bloodbath modes
	int			fraggerID;	// sprite number of dude who killed player, -1 indicated no tracking
	int			airTime;	// set when first going underwater, decremented while underwater then takes damage
	int			bloodTime;	// set when player walks through blood, decremented when on normal surface
	int			gooTime;	// set when player walks through sewage, decremented when on normal surface
	int			wetTime;	// set when player walks through water, decremented when on normal surface
	int			bubbleTime;	// set when first going underwater, decremented while underwater then takes damage
	BOOL		godMode;
	int			nLackeySprites[kMaxLackeys];
	BOOL		fScreamed;
	BOOL		fNoJump;
	int			packTime;
	int			packIndex;
	PACKITEM	packSlots[kPackMaxSlots];
	int			armor[kArmorMax];

	// view effects
	int			shake;
	int			pitch;
	int			flash;
	int			injury;
	int			blindness;
	int			drowning;
	int			itemup;
};

struct AMMOINFO
{
	int max;
	VECTOR_TYPE vectorType;
};


extern PLAYER gPlayer[kMaxPlayers], *gMe, *gView;
extern AMMOINFO gAmmoInfo[kAmmoMax];

int powerupCheck( PLAYER *pPlayer, int nPowerUp );
void powerupDraw( PLAYER *pView );
BOOL powerupActivate( PLAYER *pPlayer, int nPowerUp );
void powerupDeactivate( PLAYER *pPlayer, int nPowerUp );
void powerupProcess( PLAYER *pPlayer );
void powerupClear( PLAYER *pPlayer );
void powerupInit( void );

BOOL packAddItem( PLAYER *pPlayer, int nPackItem );
BOOL packItemActive( PLAYER *pPlayer, int nPackItem );
int  packCheckItem( PLAYER *pPlayer, int nPackItem );
void packUseItem( PLAYER *pPlayer, int nPackItem );

void playerSetRace( PLAYER *pPlayer, int nLifeMode );
void playerSetGodMode( PLAYER *pPlayer, BOOL nMode );
void playerResetInertia( PLAYER *pPlayer );
void playerStart( int nPlayer );
void playerReset( PLAYER *pPlayer );
void playerInit( int nPlayer );
void playerProcess( PLAYER *pPlayer );
void playerFireMissile( PLAYER *pPlayer, int hOff, long dx, long dy, long dz, int missileType );
SPRITE *playerFireThing( PLAYER *pPlayer, int relSlope, int thingType, int velocity );

void playerDamageSprite( int nSource, PLAYER *pPlayer, DAMAGE_TYPE nDamageType, int nDamage );
BOOL playerAddLackey( PLAYER *pPlayer, int nLackey );
void playerDeleteLackey( PLAYER *pPlayer, int nLackey );

#endif //__PLAYER_H

