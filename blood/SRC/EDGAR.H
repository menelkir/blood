/*******************************************************************************
	FILE:			EDGAR.H

	DESCRIPTION:

	AUTHOR:			Peter M. Freese
	CREATED:		02-18-96
	COPYRIGHT:		Copyright (c) 1995 Q Studios Corporation
*******************************************************************************/
#ifndef __GUIEDIT_H
#define __GUIEDIT_H

#include "gui.h"

class EButton : public TextButton
{
public:
	EButton( int left, int top, int width, int height, char *text, MODAL_RESULT result ) :
		TextButton(left, top, width, height, text, result) {};
	virtual void HandleEvent( GEVENT *event );
};

#endif


